### Set up

Manually add to `APP/config/bootstrap.php`

`Plugin::load('ApiHandler', ['routes' => true]);`

or use command

`bin/cake plugin load ApiHandler -r`

Replace manually in `APP/src/Application.php`

`use Cake\Error\Middleware\ErrorHandlerMiddleware;` → `use ApiHandler\Middleware\ErrorHandlerMiddleware;`


### Usage
#### API layer
##### Create Api Controller

Path: `APP/src/Controller/Api`

`namespace App\Controller\Api;`

`use ApiHandler\Traits\ApiController;`
```
class MyController extends AppContoller
{
    use ApiController;
    
    public function get()
    {
        $this->setResponse([1, 2, 3]);
    }
}
```

Response structure

```
{ "data": {"1", "2", "3"}, "message": null }
```

In case of exception message has a value (Exception message), data is null

##### Send requests
Prefix `api/`

In order to send request to `APP/src/Controller/Api/MyController→get()`,
use this path:

`api/my/get`
