<?php

namespace ApiHandler\Test\TestCase\Controller\Api;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Class MyControllerTest
 * @package ApiHandler\Test\TestCase\Controller\Api
 */
class MyControllerTest extends IntegrationTestCase
{
    public function testGetById()
    {
        $this->get('api/my/get-one');
        $responseJson = (string)$this->_response->getBody();
        $this->assertEquals(1, json_decode($responseJson, true)['data']);
    }

    public function testException()
    {
        $this->get('api/my/exception');
        $responseJson = (string)$this->_response->getBody();
        $this->assertEquals('Exception message', json_decode($responseJson, true)['message']);
    }
}
