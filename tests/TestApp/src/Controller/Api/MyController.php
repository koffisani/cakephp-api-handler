<?php

namespace TestApp\Controller\Api;

use ApiHandler\Traits\ApiController;
use Cake\Controller\Controller;
use Cake\Network\Exception\InternalErrorException;

class MyController extends Controller
{
    use ApiController;

    public function getOne()
    {
        $this->setResponse(1);
    }

    public function exception()
    {
        throw new InternalErrorException('Exception message');
    }
}