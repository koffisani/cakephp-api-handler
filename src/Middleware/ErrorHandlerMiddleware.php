<?php

namespace ApiHandler\Middleware;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ErrorHandlerMiddleware
 * @package ApiHandler\Middleware
 */
class ErrorHandlerMiddleware extends \Cake\Error\Middleware\ErrorHandlerMiddleware
{
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return bool
     */
    private function isRouteApi(ServerRequestInterface $request)
    {
        $path = $request->getUri()->getPath();

        $exploded = explode('/', $path);

        list($empty, $prefix) = $exploded;

        if ($prefix === 'api') {
            return true;
        }

        return false;
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param callable                                 $next
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        if ($this->isRouteApi($request)) {
            $this->exceptionRenderer = 'ApiHandler\Error\ApiExceptionRenderer';
        }

        return parent::__invoke($request, $response, $next);
    }
}
