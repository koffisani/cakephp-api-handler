<?php

namespace ApiHandler\Error;

use Cake\Error\ExceptionRenderer;

/**
 * Class ApiExceptionRenderer
 * @package ApiHandler\Error
 */
class ApiExceptionRenderer extends ExceptionRenderer
{
    /**
     * @return \Cake\Http\Response
     */
    public function render()
    {
        $this->controller->response = $this->controller->response->withType('json');
        $this->controller->response = $this->controller->response->withStatus($this->error->getCode());

        return $this->controller->response->withStringBody(json_encode([
            'data'    => null,
            'message' => $this->error->getMessage(),
        ], JSON_PRETTY_PRINT));
    }
}
