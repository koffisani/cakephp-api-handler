<?php

namespace ApiHandler\Traits;

use Cake\Event\Event;
use Cake\Network\Exception\InternalErrorException;

trait ApiController
{
    /**
     * @var array
     */
    private $jsonResponse = ['data' => null, 'message' => null];

    /**
     * @var int
     */
    private $statusCode = 200;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        parent::initialize();
    }

    /**
     * @param      $data
     * @param null $code
     * @param null $message
     */
    protected function setResponse($data, $code = null, $message = null)
    {
        $this->jsonResponse['data']    = $data;
        $this->jsonResponse['message'] = $message;

        if (! empty($code)) {
            $this->statusCode = $code;
        }
    }

    /**
     * @param \Cake\Event\Event $event
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $action = $this->request->getParam('action');

        if (method_exists($this, $action)) {
            switch ($action) {
                case 'index':
                case 'view':
                case 'read':
                    $this->request->allowMethod('get');
                    break;
                case 'create':
                case 'add':
                    $this->request->allowMethod('post');
                    $this->statusCode = 201;
                    break;
                case 'update':
                    $this->request->allowMethod(['post', 'put']);
                    break;
                case 'delete':
                    $this->request->allowMethod(['post', 'delete']);
                    break;
            }
        }

        return parent::beforeFilter($event);
    }


    /**
     * @param \Cake\Event\Event $event
     *
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if ($this->jsonResponse['data'] === null) {
            throw new InternalErrorException('Data was not set');
        }

        $this->response = $this->response->withStatus($this->statusCode);

        $this->set('jsonResponse', $this->jsonResponse);
        $this->set('_serialize', 'jsonResponse');
    }
}
